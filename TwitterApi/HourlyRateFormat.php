<?php
namespace SilexApiExample\Twitter;

class HourlyRateFormat implements IResultFormat {    
    public function format($result) {
        $tweets_per_hour = array();
        if (!empty($result)) {
            foreach ($result as $entry) {
                $hr = date('ga', strtotime($entry->created_at));
                if (isset($tweets_per_hour[$hr])) {
                    $tweets_per_hour[$hr] = $tweets_per_hour[$hr] + 1;
                } else {
                    $tweets_per_hour[$hr] = 1;
                }
            }
        }
        return $tweets_per_hour;
    }
}
