<?php
namespace SilexApiExample\Twitter;
use Abraham\TwitterOAuth\TwitterOAuth;

class TwitterApi {

    private $connection;

    public function __construct($consumer_key, $consumer_secret, $access_token, $access_secret) {
        $this->connection = new TwitterOAuth($consumer_key, $consumer_secret, $access_token, $access_secret);
        // Set if in case of proxy
        $this->connection->setProxy([
            'CURLOPT_PROXY' => FALSE,
            'CURLOPT_PROXYUSERPWD' => '',
            'CURLOPT_PROXYPORT' => 80
        ]);
    }

    public function getTweetsResult($account, $date, $format) {
        $date_since = $date;
        $date_until =  \DateTime::createFromFormat('Y-m-d', $date_since);
        $date_until->modify('+1 day');

        $content = $this->connection->get("search/tweets", [
            "from" =>$account,
            "since" => $date_since,
            "until" => $date_until,
        ]);
        
        $statuses = $content->statuses;
        $tweets_per_hour = $format->format($statuses);
        return $tweets_per_hour;
    }
}
