<?php
namespace SilexApiExample\Twitter;
interface IResultFormat {
    public function format($result);
}
