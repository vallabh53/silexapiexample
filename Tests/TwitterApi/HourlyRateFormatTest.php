<?php
require_once __DIR__ . '/../../TwitterApi/autoload.php';
use SilexApiExample\Twitter\HourlyRateFormat;
class HourlyRateFormatTest extends \PHPUnit_Framework_TestCase {
    public function testFormat() {         
        $result = array(
            new Tweet("Thu Sep 15 15:38:32 +0000 2016", "Tweet1"),
            new Tweet("Thu Sep 15 15:18:32 +0000 2016", "Tweet1"),
            new Tweet("Thu Sep 15 5:18:32 +0000 2016", "Tweet1"),
            new Tweet("Thu Sep 15 20:18:32 +0000 2016", "Tweet1"),
        );
        $format = new HourlyRateFormat($result);
        $expected = array("3pm"=>2, "5am"=>1, "8pm" => 1);
        $this->assertEquals($expected, $format->format($result));
    }
}

class Tweet {
    public $created_at;
    public $tweet;
    public function __construct($created_at, $tweet) {
        $this->created_at = $created_at;
        $this->tweet = $tweet;
    }
}