<?php
require_once __DIR__ . '/../vendor/autoload.php';

use Silex\WebTestCase;


class AppRoutingTest extends WebTestCase{
    
    private static $application;
      
    public static function setUpBeforeClass()
    {
        self::$application = require_once __DIR__.'/../app/app.php';
    }
        
    public function testIndexRoute() { 
        $client = $this->createClient();
        $client->request('GET', '/');
        $this->assertTrue($client->getResponse()->isOk());
        $this->assertContains('text/html', $client->getResponse()->headers->get('Content-Type'));
        $this->assertEquals("Try /hello/:name", $client->getResponse()->getContent());
    }
    
    public function testHelloRoute() {
        $client = $this->createClient();
        $client->request('GET', '/hello/India');
        $this->assertTrue($client->getResponse()->isOk());
        $this->assertContains('text/html', $client->getResponse()->headers->get('Content-Type'));
        $this->assertEquals("Hello India", $client->getResponse()->getContent());
    }
    
    public function testHistogramRoute() {
        $client = $this->createClient();
        $client->request('GET', '/histogram/ESPNcricinfo');
        $this->assertTrue($client->getResponse()->isOk());        
        $this->assertContains('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function createApplication() {
        return self::$application;
    }
}