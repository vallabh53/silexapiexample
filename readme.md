Silex is a PHP microframework for PHP. It is built on the shoulders of Symfony and Pimple and also inspired by sinatra.
It provides a base framework to create an API. This example contains the basic api creation examples.

Download source from git repository
https://vallabh53@bitbucket.org/vallabh53/silexapiexample.git

The directory structure as follows:
SilexApiExample
 - app/app.php
 - TwitterApi/
 - web/index.php
 - Tests
 - composer.json
 - readm.md
 
 Fill out Twitter API keys in app.php
 
Install composer
https://getcomposer.org/doc/01-basic-usage.md
 /
 Run "composer update" command to download all the dependencies.
 
 It downloads all the third party libraries including silex in a directory called "vendor", the composer.json file:
 
 {
    "name": "Silex API - Sample",
    "require": {
        "silex/silex": "~2.0",
        "abraham/twitteroauth": "^0.6.4",
        "symfony/browser-kit": "^3.1"
    }
}
 

Change the document root to the directory "SilexApiExample/web" in httpd.conf,
For example:
DocumentRoot "D:/Installed_Softwares/wamp64/www/SilexApiExample/web"

Check web server section to adjust root directory and htaccess configuration
http://silex.sensiolabs.org/doc/master/web_servers.html

Restart apache

Test API:
1. GET http://localhost/
 result: Try /hello/:name

2. GET http//localhost/hello/{name}
 result: Hello {name}

3. GET http:/histogram/{name}
result: it returns the hourly based tweets of the given day by the account


Open application in Netbeans and Run unit tests, set the unit tests path