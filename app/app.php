<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../TwitterApi/autoload.php';

use SilexApiExample\Twitter\TwitterApi;
use SilexApiExample\Twitter\HourlyRateFormat;

$app = new Silex\Application();

$app->get('/', function () {
    return 'Try /hello/:name';
});

$app->get('/hello/{name}', function ($name) use ($app) {
    return 'Hello ' . $app->escape($name);
});

$app->register(new Silex\Provider\ServiceControllerServiceProvider());

$app['twitter.api'] = function() {
	// Fill the proper api keys
    $consumer_key = "";
    $consumer_secret = "";
    $access_token = "";
    $access_secret = "";
    return new TwitterApi($consumer_key, $consumer_secret, $access_token, $access_secret);
};

$app->get('/histogram/{name}', function ($name) use ($app) {
    $errors = array();
    try {
        $twitter_api = $app['twitter.api'];
        $date = date("Y-m-d");
        $hourly_format = new HourlyRateFormat();
        return $app->json($twitter_api->getTweetsResult($app->escape($name), $date, $hourly_format));
    } catch (Exception $ex) {
        array_push($errors, $ex->getMessage());
    }
    return $app->json($errors);
});

return $app;
